//
//  TweetModel.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import Foundation

struct TweetModel {
    let tweet: String
    let tweetedBy: String
    let retweetedCount: NSNumber
}
