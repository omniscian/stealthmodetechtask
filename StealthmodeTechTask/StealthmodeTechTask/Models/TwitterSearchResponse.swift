//
//  TwitterSearchResponse.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import Foundation

struct TwitterSearchResponse {
    let tweets: [TweetModel]
    let paginationId: String
}
