//
//  TweetTableViewCell.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class TweetTableViewCell: UITableViewCell {

    @IBOutlet weak var tweetedBy : UILabel?
    @IBOutlet weak var retweetedBy : UILabel?
    @IBOutlet weak var tweetTextView : UITextView?
    @IBOutlet weak var tweetImageView : UIImageView?

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.tweetImageView?.image = tweetImageView?.image?.withRenderingMode(.alwaysTemplate)
        self.tweetImageView?.tintColor = UIColor.twitterBlueColour
    }
    
    func configure(tweet: TweetModel, delegate: UITextViewDelegate){
        
        self.tweetedBy?.text = "User: " + tweet.tweetedBy
        self.retweetedBy?.text = tweet.retweetedCount.stringValue
        self.tweetTextView?.text = tweet.tweet
        self.tweetTextView?.resolveHashTagsAndMentions()
        self.tweetTextView?.delegate = delegate
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.tweetedBy?.text = nil
        self.retweetedBy?.text = nil
        self.tweetTextView?.text = nil
        self.tweetTextView?.delegate = nil
    }
}
