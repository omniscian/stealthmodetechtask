//
//  ViewController.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

class TwitterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextViewDelegate {

    @IBOutlet weak var tableView : UITableView?
    @IBOutlet weak var textField : UITextField?
    
    let datasource : NSMutableArray = NSMutableArray.init()
    let requestManager : TwitterRequestManager = TwitterRequestManager()
    var currentSearchParam : String?
    var paginationId : String?
    
    var refreshControl = UIRefreshControl()
    var timer = Timer()
    var autoRefreshTime = 5
    var paged = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.requestTweets(searchParam:"#iOS", paginationId: nil)
        
        self.tableView?.rowHeight = UITableViewAutomaticDimension
        self.tableView?.estimatedRowHeight = 140
        
        let twitterLogo = UIImage(named: "TwitterIcon")
        let imageView = UIImageView(image:twitterLogo)  
        self.navigationItem.titleView = imageView
        self.tableView?.isHidden = true
        self.textField?.text = "#iOS"
    }
    
    func addPullToRefresh(){
        self.refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        self.refreshControl.addTarget(self, action: #selector(TwitterViewController.refresh), for: UIControlEvents.valueChanged)
        self.tableView?.addSubview(self.refreshControl)
    }

    func requestTweets(searchParam: String?, paginationId: String?) -> Void {
        
        self.currentSearchParam = searchParam
        
        self.requestManager.searchTwitterWithString(searchParam: self.currentSearchParam!, paginationId: paginationId, completion:{ (success, response) in
            
            self.refreshControl.endRefreshing()
            self.timer.invalidate()
            
            if (success){
                self.tableView?.isHidden = false
                self.datasource.addObjects(from: (response?.tweets)!)
                self.tableView?.reloadData()
                self.paginationId = response?.paginationId
                
                if (self.paged == false){
                    let scrollIndexPath = IndexPath(row: 0, section: 0)
                    self.tableView?.scrollToRow(at: scrollIndexPath as IndexPath, at: UITableViewScrollPosition.top, animated: true)
                }
                
                if (self.autoRefreshTime > 0){
                    self.timer = Timer.scheduledTimer(timeInterval: TimeInterval(self.autoRefreshTime), target:self, selector: #selector(TwitterViewController.refresh), userInfo: nil, repeats: true)
                }
            }
            else{
                self.showErrorMessage()
            }
        })
    }
    
    func refresh(){
        self.datasource.removeAllObjects()
        self.requestTweets(searchParam: self.currentSearchParam, paginationId: nil)
    }
    
    func showErrorMessage(){
        let alertController = UIAlertController(title: "Error", message: "Unable to request Tweets at the moment, please try again later", preferredStyle: UIAlertControllerStyle.alert)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func resetTimer(timeInterval: Int){
        self.autoRefreshTime = timeInterval
        self.timer.invalidate()
        self.refresh()
    }
    
    // MARK: IBActions
    
    @IBAction func refreshTimePressed(sender: UIButton){
        self.refreshControl.removeFromSuperview()
        
        let actionSheet = UIAlertController(title: "Pick duration", message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        
        actionSheet.addAction(UIAlertAction.init(title: "No refresh", style: UIAlertActionStyle.default, handler: { (action) in
            self.resetTimer(timeInterval: 0)
            self.addPullToRefresh()
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "2 seconds", style: UIAlertActionStyle.default, handler: { (action) in
            self.resetTimer(timeInterval: 2)
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "5 seconds", style: UIAlertActionStyle.default, handler: { (action) in
            self.resetTimer(timeInterval: 5)
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "30 seconds", style: UIAlertActionStyle.default, handler: { (action) in
            self.resetTimer(timeInterval: 30)
        }))
        
        actionSheet.addAction(UIAlertAction.init(title: "1 minute", style: UIAlertActionStyle.default, handler: { (action) in
            self.resetTimer(timeInterval: 60)
        }))
        
        self.present(actionSheet, animated: true, completion: nil)
    }

    
    @IBAction func searchButtonPressed(sender: UIButton){
        
        self.datasource.removeAllObjects()
        self.paged = false
        
        self.requestTweets(searchParam: self.textField?.text, paginationId: nil)
    }
    
    // MARK: UITableViewDatasource
    
    private func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.datasource.count
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "tweetCell") as! TweetTableViewCell
        
        let tweet = self.datasource.object(at: indexPath.row) as! TweetModel
        
        cell.configure(tweet: tweet, delegate: self)
        
        return cell
    }
    
    // MARK: UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastElement = self.datasource.count - 1
        if indexPath.row == lastElement {
            self.paged = true
            self.requestTweets(searchParam: self.currentSearchParam, paginationId: self.paginationId)
        }
    }
    
    // MARK: UITextViewDelegate
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        if (URL.absoluteString.hasPrefix("hash:") || URL.absoluteString.hasPrefix("mention:")) {
            var searchParam = String()
            
            if (URL.absoluteString.hasPrefix("hash:")) {
                searchParam = "#" + URL.absoluteString.replacingOccurrences(of: "hash:", with: "")
            }
            else if (URL.absoluteString.hasPrefix("mention:")) {
                searchParam = "@" + URL.absoluteString.replacingOccurrences(of: "mention:", with: "")
            }
            
            self.textField?.text = searchParam
            self.datasource.removeAllObjects()
            self.paged = false
            self.requestTweets(searchParam: searchParam, paginationId: nil)
        }
        else{
            UIApplication.shared.open(URL, options: [:], completionHandler: nil)
        }
        
        return true
    }
}

