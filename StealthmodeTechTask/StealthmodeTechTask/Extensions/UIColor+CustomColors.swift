//
//  UIColor+CustomColors.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

extension UIColor {
    public class var twitterBlueColour : UIColor{
        return UIColor(red: 0/255, green: 172/255, blue: 237/255, alpha: 1.0)
    }
}
