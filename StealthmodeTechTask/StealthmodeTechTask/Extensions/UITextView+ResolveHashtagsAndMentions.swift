
//
//  UITextView+ResolveHashtagsAndMentions.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit

extension UITextView {
    
    func resolveHashTagsAndMentions(){
        
        let originalText:NSString = self.text as NSString
        
        let allWords:[NSString] = originalText.components(separatedBy: " ") as [NSString]
        
        let attributes = [
            NSFontAttributeName : UIFont.systemFont(ofSize: 15.0)
        ]
        
        let attributedString = NSMutableAttributedString(string: text as String, attributes:attributes)
        
        for word in allWords {
            
            if word.hasPrefix("#") {
                let matchRange : NSRange = originalText.range(of: word as String)
                var string : String = word as String
                string = String(string.characters.dropFirst())
                attributedString.addAttribute(NSLinkAttributeName, value: "hash:\(string)", range: matchRange)
            }
                
            else if word.hasPrefix("@") {
                let matchRange : NSRange = originalText.range(of: word as String)
                var string : String = word as String
                string = String(string.characters.dropFirst())
                attributedString.addAttribute(NSLinkAttributeName, value: "mention:\(string)", range: matchRange)
            }
        }
        
        self.attributedText = attributedString
    }
}
