//
//  TwitterRequestManager.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 19/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import UIKit
import SwiftKeychainWrapper

class TwitterRequestManager: NSObject {
    private let baseUrlString = "https://api.twitter.com/"
    private let pageSize = 20
    
    func hasAccessToken() -> Bool{
        if (KeychainWrapper.standard.string(forKey: "accessToken") != nil){
            return true
        }
        
        return false
    }
    
    func refreshAccessToken(completion: @escaping (_: Bool) -> Void) {
        
        let credentials = "\(TwitterConstants.twitterAPIKey):\(TwitterConstants.twitterSecretKey)"
        
        let request = NSMutableURLRequest(url: NSURL(string: baseUrlString + "oauth2/token")! as URL)
        request.httpMethod = "POST"
        request.addValue("Basic \(credentials.toBase64())", forHTTPHeaderField: "Authorization")
        request.addValue("application/x-www-form-urlencoded;charset=UTF-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = "grant_type=client_credentials".data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler:{
            data, response, err -> Void in
            
            if (err == nil){
                if let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject] {
                    let accessToken = parsedData["access_token"] as! String
                    let saveSuccessful: Bool = KeychainWrapper.standard.set(accessToken, forKey: "accessToken")
                    if (saveSuccessful){
                        DispatchQueue.main.async {
                            completion(true)
                        }
                    }
                    else{
                        DispatchQueue.main.async {
                            completion(false)
                        }
                    }
                }
            }
            else{
                DispatchQueue.main.async {
                    completion(false)
                }
            }
        })
        
        task.resume()
    }
    
    func searchTwitterWithString(searchParam: String, paginationId: String?, completion: @escaping (_: Bool, _ : TwitterSearchResponse?) -> Void){
        
        if (!self.hasAccessToken()) {
            self.refreshAccessToken(completion: { (result) in
                
                if (result){
                    self.getTweets(searchParam: searchParam, paginationId: paginationId, completion: completion)
                }
                else{
                    completion(false, nil)
                }
            })
        }
        else{
            self.getTweets(searchParam: searchParam, paginationId: paginationId, completion: completion)
        }
    }
    
    func getTweets(searchParam: String, paginationId: String?, completion: @escaping (_: Bool, _ : TwitterSearchResponse?) -> Void){
        
        var urlString = baseUrlString + "1.1/search/tweets.json?q=" + searchParam.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)! + "&result_type=recent&count=20"
        
        if (paginationId != nil){
            urlString.append("&max_id=" + paginationId!)
        }
        
        let request = NSMutableURLRequest(url: NSURL(string: urlString)! as URL)
        request.httpMethod = "GET"
        request.addValue("Bearer " + KeychainWrapper.standard.string(forKey: "accessToken")!, forHTTPHeaderField: "Authorization")
        
        let session = URLSession.shared
        
        let task = session.dataTask(with: request as URLRequest, completionHandler:{
            data, response, err -> Void in
            
            var tweetsArray = [TweetModel]()
            var paginationId = String()
            
            if let parsedData = try? JSONSerialization.jsonObject(with: data!) as! [String:AnyObject] {
                
                if ((parsedData["errors"]) != nil){
                    KeychainWrapper.standard.removeObject(forKey: "accessToken")
                    DispatchQueue.main.async {
                        completion(false, nil)
                    }
                }
                else{
                    let statuses = parsedData["statuses"] as! NSArray
                    
                    for (index, _) in statuses.enumerated() {
                        
                        let status = statuses[index] as! [String:AnyObject]
                        
                        let tweet = TweetModel(tweet: status["text"] as! String, tweetedBy: status["user"]!["screen_name"] as! String, retweetedCount: status["retweet_count"] as! NSNumber)
                        tweetsArray.append(tweet)
                        
                        if (index == (statuses.count - 1)){
                            var tweetId = status["id"] as! Int
                            tweetId = tweetId - 1
                            paginationId = String(tweetId)
                        }
                    }
                    
                }
                
                let twitterSearchResponse = TwitterSearchResponse(tweets: tweetsArray, paginationId: paginationId)
                
                DispatchQueue.main.async {
                    completion(true, twitterSearchResponse)
                }
            }
            
            
        })
        
        task.resume()
    }
}
