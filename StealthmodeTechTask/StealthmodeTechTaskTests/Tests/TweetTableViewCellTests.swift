//
//  TweetTableViewCellTests.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 20/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import XCTest

class TweetTableViewCellTests: XCTestCase, UITextViewDelegate {
    
    let cell = TweetTableViewCell()
    
    func testAwakeFromNib(){
        
        let imageView = UIImageView(image: UIImage(named:"RetweetIcon"))
        
        self.cell.tweetImageView = imageView
        self.cell.awakeFromNib()
        
        XCTAssertEqual(self.cell.tweetImageView?.tintColor, UIColor.twitterBlueColour)
        XCTAssertEqual(self.cell.tweetImageView?.image?.renderingMode, .alwaysTemplate)
    }
    
    func testConfigure(){
        let tweetLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        let retweetLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        
        self.cell.tweetedBy = tweetLabel
        self.cell.retweetedBy = retweetLabel
        self.cell.tweetTextView = UITextView()
        
        self.cell.configure(tweet: TweetModel(tweet: "TestTweet", tweetedBy: "Username", retweetedCount: 5), delegate: self as UITextViewDelegate)
        
        XCTAssertEqual(self.cell.tweetedBy?.text, "User: Username")
        XCTAssertEqual(self.cell.retweetedBy?.text, "5")
        XCTAssertEqual(self.cell.tweetTextView?.text, "TestTweet")
    }

}
