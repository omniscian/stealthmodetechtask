//
//  TwitterRequestManagerTests.swift
//  StealthmodeTechTask
//
//  Created by Ian Houghton on 20/11/2016.
//  Copyright © 2016 YakApps. All rights reserved.
//

import XCTest
import SwiftKeychainWrapper

class TwitterRequestManagerTests: XCTestCase {
    
    let requestManager : TwitterRequestManager = TwitterRequestManager()
    
    func testHasAccessToken(){
        KeychainWrapper.standard.removeObject(forKey: "accessToken")
        XCTAssertFalse(self.requestManager.hasAccessToken())
        
        KeychainWrapper.standard.set("accessToken", forKey: "accessToken")
        XCTAssertTrue(self.requestManager.hasAccessToken())
        
        KeychainWrapper.standard.removeObject(forKey: "accessToken")
    }
}
